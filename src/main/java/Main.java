import atm.CashMachine;

public class Main {

    public static void main(String[] args) {

        CashMachine atm = CashMachine.getInstance();

        atm.run();
    }
}
