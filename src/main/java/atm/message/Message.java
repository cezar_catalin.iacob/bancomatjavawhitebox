package atm.message;

public class Message {
    private MessageType messageType;
    private String messageContents;
    private MessageDestination messageDestination;

    public Message (MessageType messageType, String messageContents, MessageDestination messageDestination) {
        this.messageType = messageType;
        this.messageContents = messageContents;
        this.messageDestination = messageDestination;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public String getMessageContents() {
        return messageContents;
    }

    public MessageDestination getMessageDestination() {
        return messageDestination;
    }

    public void send() {
        messageDestination
                .getNotificationService()
                .send(messageType.getMessageTypePrefix() + messageContents );}
}
