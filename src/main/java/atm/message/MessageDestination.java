package atm.message;

import atm.services.notification.NotificationConsoleService;
import atm.services.notification.NotificationMailService;
import atm.services.notification.NotificationSmsService;
import atm.services.notification.SendNotificationService;

public enum MessageDestination {
    MAIL() {
        @Override
        public SendNotificationService getNotificationService() {
            return new NotificationMailService();
        }
    },
    SMS() {
        @Override
        public SendNotificationService getNotificationService() {
            return new NotificationSmsService();
        }
    },
    CONSOLE() {
        @Override
        public SendNotificationService getNotificationService() {
            return new NotificationConsoleService();
        }
    };

    public abstract SendNotificationService getNotificationService();
}
