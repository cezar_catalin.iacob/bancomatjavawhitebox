package atm.message;

public enum MessageType {
    INFO(""),
    IMPORTANT("NOTE: "),
    WARNING("WARNING: "),
    CRITICAL("CRITICAL: "),
    ERROR("ERROR: ");

    private final String messageTypePrefix;

    MessageType(String messageTypePrefix) { this.messageTypePrefix = messageTypePrefix; }

    public String getMessageTypePrefix() {
        return messageTypePrefix;
    }
}
