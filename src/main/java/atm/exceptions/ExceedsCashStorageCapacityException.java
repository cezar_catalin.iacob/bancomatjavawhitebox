package atm.exceptions;

public class ExceedsCashStorageCapacityException extends Exception {
    public ExceedsCashStorageCapacityException(String message) {
        super(message);
    }
}
