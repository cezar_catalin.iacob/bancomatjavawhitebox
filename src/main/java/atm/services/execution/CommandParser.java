package atm.services.execution;

import atm.exceptions.InvalidInputException;

public class CommandParser {

    public static Integer parse(String command) throws InvalidInputException {
        if(command.matches("^\\d+$")) {
            return Integer.parseInt(command);
        }
        else {
            throw new InvalidInputException("Invalid input !\r\n");
        }
    }
}
