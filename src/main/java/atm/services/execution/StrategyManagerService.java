package atm.services.execution;

import atm.strategies.banknote_stock_strategies.BanknoteStockStrategy;
import atm.strategies.user_request_result_strategies.UserRequestResultStrategy;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.Set;

public class StrategyManagerService {



    private Set<Class<? extends BanknoteStockStrategy>> banknoteStockStrategyClasses =
            new Reflections(BanknoteStockStrategy.class.getPackageName())
                    .getSubTypesOf(BanknoteStockStrategy.class);

    Set<Class<? extends UserRequestResultStrategy>> userRequestResultStrategyClasses =
            new Reflections(UserRequestResultStrategy.class.getPackageName())
                    .getSubTypesOf(UserRequestResultStrategy.class);

    public void handleUserRequestResultStrategies(Integer amount) {
        this.userRequestResultStrategyClasses.stream()
                .map(aClass -> {
                    try {
                        return (UserRequestResultStrategy) aClass
                                .getDeclaredConstructor()
                                .newInstance();
                    } catch (NoSuchMethodException |
                            IllegalAccessException |
                            InvocationTargetException |
                            InstantiationException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .filter(userRequestResultStrategy -> userRequestResultStrategy.check(amount))
                .forEach(UserRequestResultStrategy::execute);
    }

    public void handleBanknoteStockStrategies() {
        this.banknoteStockStrategyClasses.stream()
                .map(aClass -> {
                    try {
                        return (BanknoteStockStrategy) aClass
                                .getDeclaredConstructor()
                                .newInstance();
                    } catch (NoSuchMethodException |
                            IllegalAccessException |
                            InvocationTargetException |
                            InstantiationException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .filter(BanknoteStockStrategy::check)
                .forEach(BanknoteStockStrategy::execute);
    }

}
