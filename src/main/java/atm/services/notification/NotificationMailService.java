package atm.services.notification;

import atm.io.ConsoleIO;
import atm.io.InOut;

public class NotificationMailService implements SendNotificationService {
    @Override
    public void send(String s) {
        InOut io = new ConsoleIO();
        io.writeOutput(s);
    }
}
