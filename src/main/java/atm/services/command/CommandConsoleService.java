package atm.services.command;

import atm.io.ConsoleIO;
import atm.io.InOut;

public class CommandConsoleService implements RecieveCommandService {
    @Override
    public String receiveCommand() {
        InOut io = new ConsoleIO();
        return io.readInput();
    }
}
