package atm.strategies.user_request_result_strategies;

import atm.message.Message;
import atm.message.MessageDestination;
import atm.message.MessageType;

public class UserRequestToRetrieveCashExceedsTwoHundredRonStrategy implements UserRequestResultStrategy {
    @Override
    public void execute() {
        new Message(MessageType.IMPORTANT,
                "Sum of cash retrieved exceeds 200RON !\r\n",
                MessageDestination.CONSOLE).send();
    }

    @Override
    public boolean check(Integer amount) {
        return amount > 200 ;
    }
}
