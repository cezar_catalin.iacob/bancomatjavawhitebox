package atm.strategies.user_request_result_strategies;

public interface UserRequestResultStrategy {
    void execute();
    boolean check(Integer amount);
}
