package atm.strategies.banknote_stock_strategies;

public interface BanknoteStockStrategy {
    void execute();
    boolean check();
}
