package atm.strategies.banknote_stock_strategies;

import atm.CashMachine;
import atm.cash.BanknoteType;
import atm.message.Message;
import atm.message.MessageDestination;
import atm.message.MessageType;

public class FiftyRonBanknotesBelowFifteenPercentStrategy implements BanknoteStockStrategy {

    @Override
    public void execute() {
        new Message(MessageType.WARNING,
                "The amount of 50 RON banknotes is below 15%\r\n",
                MessageDestination.MAIL).send();
    }

    @Override
    public boolean check() {
        int nrBanknotes = CashMachine.getInstance().getVault().queryCashStock(BanknoteType.FIFTY_RON);
        int maxCapacity = CashMachine.getInstance().getVault().getMaxCapacity(BanknoteType.FIFTY_RON);
        return (nrBanknotes < maxCapacity * 15/100);
    }

}
