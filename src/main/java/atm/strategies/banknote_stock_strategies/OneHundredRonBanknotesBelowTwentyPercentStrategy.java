package atm.strategies.banknote_stock_strategies;

import atm.CashMachine;
import atm.cash.BanknoteType;
import atm.message.Message;
import atm.message.MessageDestination;
import atm.message.MessageType;
import atm.services.notification.NotificationMailService;
import atm.services.notification.SendNotificationService;

public class OneHundredRonBanknotesBelowTwentyPercentStrategy implements BanknoteStockStrategy {

    @Override
    public void execute() {
        new Message(MessageType.WARNING,
                "The amount of 100 RON banknotes is below 20%\r\n",
                MessageDestination.MAIL).send();
    }

    @Override
    public boolean check() {
        int nrBanknotes = CashMachine.getInstance().getVault().queryCashStock(BanknoteType.ONE_HUNDRED_RON);
        int maxCapacity = CashMachine.getInstance().getVault().getMaxCapacity(BanknoteType.ONE_HUNDRED_RON);
        return (nrBanknotes < maxCapacity * 20/100);
    }

}
