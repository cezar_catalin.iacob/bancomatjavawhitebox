package atm.cash;

import atm.CashMachine;
import atm.exceptions.ExceedsCashStorageCapacityException;
import atm.exceptions.InsufficientCashException;

import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class CashStorage implements Iterable<CashStorage.Entry> {

    public static class Entry {
        private BanknoteType banknoteType;
        private Integer nrBanknotes;

        public Entry(Map.Entry<BanknoteType,Integer> entry) {
            this.banknoteType = entry.getKey();
            this.nrBanknotes  = entry.getValue();
        }
        public Entry(BanknoteType banknoteType, Integer nrBanknotes) {
            this.banknoteType = banknoteType;
            this.nrBanknotes = nrBanknotes;
        }

        public BanknoteType getBanknoteType() {
            return this.banknoteType;
        }

        public int getNumberOfBanknotes() {
            return this.nrBanknotes;
        }

    }

    private SortedMap<BanknoteType,Integer> storage;

    private Map<BanknoteType,Integer> maxCapacity;

    public CashStorage() {
        this.storage = new TreeMap<>();
        this.maxCapacity = new TreeMap<>();
        //Default values for maxCapacity
        this.maxCapacity.put(BanknoteType.ONE_RON,100);
        this.maxCapacity.put(BanknoteType.FIVE_RON,100);
        this.maxCapacity.put(BanknoteType.TEN_RON,100);
        this.maxCapacity.put(BanknoteType.FIFTY_RON,50);
        this.maxCapacity.put(BanknoteType.ONE_HUNDRED_RON,50);
    }

    public CashStorage(Map<BanknoteType,Integer> maxCapacity) {
        this.storage = new TreeMap<>();
        this.setMaxCapacity(maxCapacity);
    }

    public Integer totalValueInStorage() {
        return storage.entrySet().stream()
                .mapToInt(entry -> (entry.getValue() * entry.getKey().value))
                .sum();
    }

    public SortedMap<BanknoteType,Integer> retrieveCash(int total_value_to_retrieve) throws InsufficientCashException {
        SortedMap<BanknoteType,Integer> pouch = new TreeMap<>();
        int value_left_to_retrieve = total_value_to_retrieve;
        for (CashStorage.Entry entry : this) {
            int unit_value = entry.banknoteType.value;
            int amount_available = entry.nrBanknotes;
            int amount_retrievable = Integer.min(amount_available,value_left_to_retrieve/unit_value);
            pouch.put(entry.getBanknoteType(),amount_retrievable);
            value_left_to_retrieve -= unit_value * amount_retrievable;
        }
        if(value_left_to_retrieve > 0) {
            throw new InsufficientCashException("Insufficient cash !\r\n");
        }
        for (SortedMap.Entry<BanknoteType,Integer> entry : pouch.entrySet()) {
            storage.put( entry.getKey(),
                    storage.get(entry.getKey()) - entry.getValue() );
        }
        return pouch;
    }

    public void setMaxCapacity(Map<BanknoteType,Integer> maxCapacity) {
        this.maxCapacity = maxCapacity;
    }
    public void setMaxCapacity(BanknoteType banknoteType, Integer nrBanknotes) {
        this.maxCapacity.put(banknoteType,nrBanknotes);
    }

    public Map<BanknoteType,Integer> getMaxCapacity() {
        return this.maxCapacity;
    }
    public Integer getMaxCapacity(BanknoteType banknoteType) {
        return this.maxCapacity.get(banknoteType);
    }

    public SortedMap<BanknoteType,Integer> queryCashStock() {
        return this.storage;
    }
    public Integer queryCashStock(BanknoteType banknoteType) {
        return this.storage.get(banknoteType);
    }

    public void fillStorage() {
        this.storage.put(BanknoteType.ONE_RON,this.maxCapacity.get(BanknoteType.ONE_RON));
        this.storage.put(BanknoteType.FIVE_RON,this.maxCapacity.get(BanknoteType.FIVE_RON));
        this.storage.put(BanknoteType.TEN_RON,this.maxCapacity.get(BanknoteType.TEN_RON));
        this.storage.put(BanknoteType.FIFTY_RON,this.maxCapacity.get(BanknoteType.FIFTY_RON));
        this.storage.put(BanknoteType.ONE_HUNDRED_RON,this.maxCapacity.get(BanknoteType.ONE_HUNDRED_RON));
    }
    public void fillStorage(SortedMap<BanknoteType,Integer> cashToAdd)
            throws ExceedsCashStorageCapacityException {
        for (Map.Entry<BanknoteType,Integer> entry : cashToAdd.entrySet()) {
            if (entry.getValue() > this.maxCapacity.get(entry.getKey())) {
                throw new ExceedsCashStorageCapacityException("Cash storage capacity exceeded !\r\n");
            }
            this.storage.put(entry.getKey(), entry.getValue());
        }
    }
    public void fillStorage(BanknoteType banknoteType, Integer nrBanknotesToAdd)
            throws ExceedsCashStorageCapacityException {
        if(nrBanknotesToAdd > this.maxCapacity.get(banknoteType)) {
            throw new ExceedsCashStorageCapacityException("Cash storage capacity exceeded !\r\n");
        }
        this.storage.put(banknoteType,nrBanknotesToAdd);
    }

    @Override
    public Iterator<CashStorage.Entry> iterator() {
        Iterator<Map.Entry<BanknoteType,Integer>> iter = storage.entrySet().iterator();
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return iter.hasNext();
            }

            @Override
            public CashStorage.Entry next() {
                return new Entry(iter.next());
            }
        };
    }
}
