package atm.cash;

public enum BanknoteType {
    ONE_HUNDRED_RON(100),
    FIFTY_RON(50),
    TEN_RON(10),
    FIVE_RON(5),
    ONE_RON(1);

    public final int value;

    BanknoteType(int value) {
        this.value = value;
    }
}