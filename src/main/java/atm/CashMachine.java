package atm;

import atm.cash.CashStorage;
import atm.exceptions.ExceedsCashStorageCapacityException;
import atm.exceptions.InsufficientCashException;
import atm.exceptions.InvalidInputException;
import atm.message.Message;
import atm.message.MessageDestination;
import atm.message.MessageType;
import atm.services.command.CommandConsoleService;
import atm.services.command.RecieveCommandService;
import atm.services.execution.CommandParser;
import atm.services.execution.StrategyManagerService;

public class CashMachine {

    private static CashMachine instance;

    private CashStorage vault;

    private CashMachine() {
        this.vault = new CashStorage();
        this.vault.fillStorage();

    }

    public synchronized static CashMachine getInstance() {
        if(CashMachine.instance == null) {
            CashMachine.instance = new CashMachine();
        }
        return CashMachine.instance;
    }

    public CashStorage getVault() {
        return this.vault;
    }

    public void run() {
        RecieveCommandService commandService = new CommandConsoleService();
        StrategyManagerService strategyManagerService = new StrategyManagerService();
        while(true) {
            new Message(MessageType.INFO,
                    "Input the sum of money to retrieve: ",
                    MessageDestination.CONSOLE).send();
            Integer amountOfCash;
            CashStorage pouch = new CashStorage();
            try {
                amountOfCash = CommandParser.parse(commandService.receiveCommand());
                pouch.fillStorage(vault.retrieveCash(amountOfCash));
            } catch (InvalidInputException |
                    InsufficientCashException |
                    ExceedsCashStorageCapacityException e) {
                new Message(MessageType.ERROR,
                        e.getMessage(),
                        MessageDestination.CONSOLE).send();
                continue;
            }
            new Message(MessageType.INFO,
                    pouch.queryCashStock().entrySet().toString()+"\r\n",
                    MessageDestination.CONSOLE).send();
            strategyManagerService.handleUserRequestResultStrategies(amountOfCash);
            strategyManagerService.handleBanknoteStockStrategies();
        }
    }
}
