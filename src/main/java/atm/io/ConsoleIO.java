package atm.io;

import java.util.Scanner;

public class ConsoleIO implements InOut {

    private static Scanner scanner;

    public ConsoleIO() {
        if(scanner == null) {
            ConsoleIO.scanner = new Scanner(System.in);
            ConsoleIO.scanner.useDelimiter("\n");
        }
    }

    @Override
    public String readInput() {
        return ConsoleIO.scanner.next();
    }

    @Override
    public void writeOutput(String s) {
        System.out.print(s);
    }
}
