package atm.io;

public interface InOut {
    String readInput();
    void writeOutput(String s);
}
